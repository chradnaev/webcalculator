<%@page language="java" contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="html" uri="/WEB-INF/struts-html.tld" %>
<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld" %>
<%@taglib prefix="bean" uri="/WEB-INF/struts-bean.tld" %>
<html>
<head>
    <style>
        .container {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: center;
            width: 100%;
            height: 100%;
        }
        .form {
            background: #6feeff;
            border-radius: 3px;
        }
        .errors {
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <h2>Hello, Username! </h2>
    <section class="form">
        <div class="errors">
            <%=request.getAttribute("error") == null ? "" : request.getAttribute("error")%>
        </div>
        <html:form action="calc.do">
            <label>operand 1: <html:text property="operand1" /></label>
            <label>operation:
            <html:select property="operation" value="*" >
                <html:option value="*" >*</html:option>
                <html:option value="/" >/</html:option>
                <html:option value="+" >+</html:option>
                <html:option value="-" >-</html:option>
            </html:select>
            </label>
            <label>operand 2: <html:text property="operand2" /><html:errors property="operand2" /></label>
            <bold>=</bold><html:text property="result" />
            <html:submit property="method" value="Calc" />
        </html:form>
    </section>
</div>
</body>
</html>
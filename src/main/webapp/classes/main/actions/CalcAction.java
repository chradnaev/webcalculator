package main.actions;

import main.forms.CalcForm;
import org.apache.struts.action.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalcAction extends Action {
    private Pattern mulDivPattern = Pattern.compile("(?<f>\\d+(\\.\\d+)?)(?<op>[*/])(?<s>\\d+(\\.\\d+)?)");
    private Pattern plusMinusPattern = Pattern.compile("(?<f>\\d+(\\.\\d+)?)(?<op>[\\+\\-])(?<s>\\d+(\\.\\d+)?)");

    public CalcAction() {

    }

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
        CalcForm cform = (CalcForm) form;
        System.out.println(cform.toString());
        switch (cform.getOperation()) {
            case '*':
                cform.setResult(cform.getOperand1() * cform.getOperand2());
                break;
            case '/':
                if (cform.getOperand2() == 0) {
                    System.out.println("error");
                    request.setAttribute("error", "Zero division");
                } else {
                    cform.setResult(cform.getOperand1() / cform.getOperand2());
                }
                break;
            case '+':
                cform.setResult(cform.getOperand1() + cform.getOperand2());
                break;
            case '-':
                cform.setResult(cform.getOperand1() - cform.getOperand2());
                break;
            default:
                throw new IllegalArgumentException("Incorrect operation");
        }

        return mapping.findForward("index");
    }

    public double calcExpression(String expr) {
        int offset;
        StringBuffer exprBuffer = new StringBuffer(expr);
        Queue<int[]> brackets = getBracketLocations(exprBuffer);
        return 0;
    }
    public Queue<int[]> getBracketLocations(StringBuffer expr) {
        Queue<int[]> bracketQueue = new LinkedBlockingQueue<>();
        Deque<Integer> bracketBegins = new LinkedBlockingDeque<>();
        for (int i = 0; i < expr.length(); i++) {
            if (expr.charAt(i) == '(') {
                bracketBegins.addLast(i);
            } else if (expr.charAt(i) == ')') {
                int begin = bracketBegins.removeLast();
                bracketQueue.add(new int[] {begin, i});
            }
        }
        if (!bracketBegins.isEmpty()) {
            throw new IllegalArgumentException("Incorrect brackets");
        }
        return bracketQueue;
    }
    public double calcBracketlessExpression(StringBuffer expr) {
        return 0;
    }
    private double replaceByPattern(StringBuffer expr, Pattern pattern) {
        Matcher matcher = pattern.matcher(expr);
        while (matcher.find()) {
            double first = Double.parseDouble(matcher.group("f"));
            double second = Double.parseDouble(matcher.group("s"));
            char op = matcher.group("op").charAt(0);
            double result;
            switch (op) {
                case '/': result = first / second; break;
                case '*': result = first * second; break;
                case '+': result = first + second; break;
                case '-': result = first - second; break;
                default: throw new IllegalArgumentException("Illegal operation");
            }
            expr.replace(matcher.start(), matcher.end(), Double.toString(result));
            matcher = pattern.matcher(expr);
        }
        return 0;
    }

}
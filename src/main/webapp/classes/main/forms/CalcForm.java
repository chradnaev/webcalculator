package main.forms;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;


public class CalcForm extends ActionForm {
    private double operand1;
    private double operand2;
    private Character operation;
    private double result;
    private String expression = "qwerty";
    private String error;

    public CalcForm() {
        super();
    }

    public CalcForm(double operand1, double operand2, Character op) {
        this();
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operation = op;
    }

    /*@Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = super.validate(mapping, request);
        System.out.println(this.toString());
        System.out.println(new ActionMessage("errors.operand2.zero"));
        if (operation != null && operation == '/' && operand2 == 0) {
            errors.add("operand2", new ActionMessage("errors.operand2.zero"));
        }
        return errors;
    }*/
/*@Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = super.validate(mapping, request);
        System.out.println(this.toString());
        if (operation != null && operation == '/' && operand2 == 0) {
            errors.add("textfield", new ActionMessage("error.operand2.required"));
        }
        return errors;
    }*/

    public double getOperand1() {
        return operand1;
    }

    public void setOperand1(double operand1) {
        this.operand1 = operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public void setOperand2(double operand2) {
        this.operand2 = operand2;
    }

    public Character getOperation() {
        return operation;
    }

    public void setOperation(Character operation) {
        this.operation = operation;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CalcForm{" +
            "operand1=" + operand1 +
            ", operand2=" + operand2 +
            ", operation=" + operation +
            ", result=" + result +
            '}';
    }
}

<%@page language="java" contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>

<html>
<head>
    <style>
        container {
            display: flex;
            flex-direction: row;
            width: 70%;
            height: 70%;
            border: 1px solid red;
        }
    </style>
</head>
<body>
<h2>Hello, <bean:write name="calcForm" property="name"/></h2>
<section class="container">
    <!--<form action="/">
        <label for="">Name:<input type="text" name="name"></label>
    </form>-->
</section>
</body>
</html>